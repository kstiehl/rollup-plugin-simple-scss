# rollup-simple-scss

This project started as a fork of [rollup-plugin-scss][link-original-project] but has received several improvements to make it easier to use and to be more compliant with the rollup way of doing things.
It is now easier to integrate this plugin with existing frameworks like `VueJS`, `Angular`, and `React`.

<a href="LICENSE">
  <img src="https://img.shields.io/badge/license-MIT-brightgreen.svg" alt="Software License" />
</a>
<a href="https://github.com/kstiehl/rollup-plugin-simple-scss/issues">
  <img src="https://img.shields.io/github/issues/kstiehl/rollup-plugin-simple-scss.svg" alt="Issues" />
</a>
<a href="http://standardjs.com/">
  <img src="https://img.shields.io/badge/code%20style-eslint-brightgreen.svg" alt="JavaScript Style Guide" />
</a>
<a href="https://github.com/kstiehl/rollup-plugin-simple-scss/releases">
  <img src="https://img.shields.io/github/release/kstiehl/rollup-plugin-simple-scss" alt="Latest Version" />
</a>
<img src="https://img.shields.io/npm/dm/rollup-plugin-simple-scss"/>

## Installation
```
# Rollup v0.60+ and v1+
npm install --save-dev rollup-plugin-simple-scss
```

## Usage
```JavaScript
// rollup.config.js
import scss from 'rollup-plugin-simple-scss'

export default {
   input: 'input.js',
  output: {
    file: 'output.js',
    format: 'iife'
  },
  plugins: [
    scss()
  ]
}
```

```js
// entry.js
import './reset.scss'
```

This would parse and minify `reset.scss` and if the CSS output is small enough it will be inlined using data URLs otherwise a new CSS file will be included in the build output.

It is also possible to receive the URL which can be used to fetch this file which makes it compatible with `VueJS`, `Angular` and `React`.

Here an React example

```JavaScript
import style from './reset.scss';

  ...
  <link rel='style' href={style}>
  ...

```

### Receiving CSS output as a string

If you need the css output as a string in your javascript code and don't want to produce an additional css output file you can do this by adding the `content:` prefix to the import.

```JavaScript
import style from 'content:./reset.scss';

console.log(style);
```

This would print the parsed css output.

### Options

Options are passed to [node-sass], so that you can include all the options which [node-sass] supports.

```js
scss({
  // specify the maximum size(byte). 
  // CSS output which does not exceed the limit will be inlined using
  // data URLs.
  limit: 4096
  // Determine if build process should be terminated on error (default: false)
  failOnError: true,
})
```

## Contributing

Contributions and feedback are very welcome.

To get it running:
  1. Clone the project.
  2. `npm install`

## Credits

- [Thomas Ghysels](https://github.com/thgh)
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE) for more information.

[link-original-project]: https://github.com/thgh/rollup-plugin-scss
[link-contributors]: ../../contributors
[node-sass]: https://www.npmjs.com/package/node-sass