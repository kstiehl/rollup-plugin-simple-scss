import scss from '../index.es.js'

export default {
  input: 'input.js',
  output: {
    file: 'output.js',
    format: 'iife'
  },
  plugins: [
    scss({limit: 1})
  ]
}
