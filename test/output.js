(function () {
	'use strict';

	var css = new URL('assets/input-ef509c42.scss', document.currentScript && document.currentScript.src || document.baseURI).href;

	const link = document.createElement('link');
	link.rel = 'stylesheet';
	link.href = css;
	document.body.append(link);
	console.log('scss imported', css);

}());
