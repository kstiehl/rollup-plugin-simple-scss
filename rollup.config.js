import { module, main } from './package.json';
export default {
    input: 'index.es.js',
    output: [{
        file: main,
        format: 'cjs'
    }, {
        file: module,
        format: 'es'
    }],
};
