import { basename } from 'path';
import { createFilter } from 'rollup-pluginutils';
import { readFile as fread } from 'fs';
import { promisify } from 'util';
import { renderSync } from 'node-sass';

export default function css(options = {}) {
    const CONENT_URI = 'content:';
    const filter = createFilter(options.include || ['/**/*.css', '/**/*.scss', '/**/*.sass'], options.exclude, false);
    const readFile = promisify(fread);
    const limit = options.limit == undefined ? 4096 : options.limit; // create dataURLS for 4kb css code.
    let includePaths = options.includePaths || [];
    includePaths.push(process.cwd());

    const compileToCSS = function (scss) {
        // Compile SASS to CSS
        if (scss.length) {
            includePaths = includePaths.filter((v, i, a) => a.indexOf(v) === i);
            try {
                const css = renderSync(Object.assign({
                    data: scss,
                    outputStyle: 'compressed',
                    includePaths
                }, options)).css.toString();
                return css;
            } catch (e) {
                if (options.failOnError) {
                    throw e;
                }
                console.log();
                console.log(red('Error:\n\t' + e.message));
                if (e.message.includes('Invalid CSS')) {
                    console.log(green('Solution:\n\t' + 'fix your Sass code'));
                    console.log('Line:   ' + e.line);
                    console.log('Column: ' + e.column);
                }
                if (e.message.includes('node-sass') && e.message.includes('find module')) {
                    console.log(green('Solution:\n\t' + 'npm install --save node-sass'));
                }
                if (e.message.includes('node-sass') && e.message.includes('bindigs')) {
                    console.log(green('Solution:\n\t' + 'npm rebuild node-sass --force'));
                }
                console.log();
            }
        }
    };

    return {
        name: 'simple-scss',
        async resolveId(id, importer) {
            // make sure relative paths work with CONTENT_URI
            if (id.startsWith(CONENT_URI)) {
                return this.resolve(id.slice(CONENT_URI.length), importer).then(({id : resolvedId}) => {
                    return CONENT_URI+resolvedId;
                });
            }
            return null;
        },

        async load(id) {
            if (!(filter(id) || id.startsWith(CONENT_URI) && filter(id.slice(CONENT_URI.length)))) {
                return;
            }
            // check if content should be injected instead of the file url;
            let injectCSS = false;
            if (id.startsWith(CONENT_URI)) {
                id = id.slice(CONENT_URI.length);
                injectCSS = true;
            }
            const scss = await readFile(id).then();
            let css = compileToCSS(scss.toString());
            if (injectCSS) {
                return `export default ${JSON.stringify(css)}`;
            } else if (css.length <= limit) {
                css = Buffer.from(css).toString('base64');
                return `export default 'data:text/css;base64,${css}';`;
            } else {
                const referenceId = this.emitFile({
                    type: 'asset',
                    name: basename(id),
                    source: css
                });
                return `export default import.meta.ROLLUP_ASSET_URL_${referenceId}`;
            }
        }
    };
}

function red(text) {
    return '\x1b[1m\x1b[31m' + text + '\x1b[0m';
}

function green(text) {
    return '\x1b[1m\x1b[32m' + text + '\x1b[0m';
}